#include <Arduino.h>
#include "myFunctions.h"

uint16_t DisplayErrorScreen = 0;
uint32_t tempTicks = 0;

void setup() {

	configIO();

	Wire.setClock(1000);

	Mux_LCD_init();

	ScanI2CForDevices();    //if pressure sensors are not found, the system will be stopped here!

	// Timer0 is already used for millis() - we'll just interrupt somewhere
	// in the middle and call the "Compare A" function below
	OCR0A = 0xAF;
	TIMSK0 |= _BV(OCIE0A);

	TCCR1A = 0x00;         // COM1A1=0, COM1A0=0 => Disconnect Pin OC1 from Timer/Counter 1 -- PWM11=0,PWM10=0 => PWM Operation disabled
	TCCR1B = 0x05;         // 16MHz clock with prescaler means TCNT1 increments every 64 uS (cs12 and CS10 bit set)
	Ticks = 0;             // default value indicating no pulse detected
	TIMSK1 = _BV(ICIE1);   // enable input capture interrupt for timer 1

	Mux_LCD_clear();
}

SIGNAL(TIMER0_COMPA_vect)
{

	if(DisplayCounter < (LCD_TICKER_S * 1000))
	{
		DisplayCounter++;
	}
}

ISR(TIMER1_CAPT_vect){
  if( bit_is_set(TCCR1B ,ICES1)){       // was rising edge detected ?
      TCNT1 = 0;                        // reset the counter
  }
  else {                                // falling edge was detected
       Ticks = ICR1;
  }
  TCCR1B ^= _BV(ICES1);                 // toggle bit value to trigger on the other edge
}


void loop() {

	MeasurementData.WaterTempBT = TempLookup(analogRead(ADC_WT_BT));
	MeasurementData.WaterTempAT = TempLookup(analogRead(ADC_WT_AT));
	MeasurementData.Data1Filter = ReadPressureAndTemperature(PRESSURESENSOR1);
	MeasurementData.Data2Filter = ReadPressureAndTemperature(PRESSURESENSOR2);
	MeasurementData.Data3Filter = ReadPressureAndTemperature(PRESSURESENSOR3);
	MeasurementData.Data4Filter = ReadPressureAndTemperature(PRESSURESENSOR4);
	MeasurementData.PresFilter1 = MeasurementData.Data2Filter.pressure - MeasurementData.Data1Filter.pressure;
	MeasurementData.PresFilter2 = MeasurementData.Data3Filter.pressure - MeasurementData.Data2Filter.pressure;
	MeasurementData.PresFilter3 = MeasurementData.Data4Filter.pressure - MeasurementData.Data3Filter.pressure;
	tempTicks = getTick();
	if(tempTicks != 0)
	{
		MeasurementData.WaterFlow = FlowLookup(10000000L / (2 * tempTicks * 64));
	}

	ErrorHandling();

	if(DisplayCounter >= (LCD_TICKER_S * 1000))
	{
	if(Errors == 0)
	{
		if(DisplayScreen < LCD_SCREENS-1)
		{
		  DisplayScreen++;
		}
		else
		  DisplayScreen = 0;
	}
	else
	{
		for(int i = 0; i < MAX_ERRORS; i++)
		{
			DisplayScreen = ERR_BASE + (Errors & (1<<DisplayErrorScreen));
			DisplayErrorScreen++;

			if(DisplayErrorScreen == 7)
				DisplayErrorScreen = 0;

			if(DisplayScreen != ERR_BASE)
				break;
		}

	}

	DisplayCounter = 0;
	Mux_LCD_clear();
	}
	LCD_Display();

}
